# Android Modules
<div align="center">
    <a href="https://kotlinlang.org"><img src="https://img.shields.io/badge/Kotlin-1.4.21-orange?logo=kotlin&logoColor=blue&style=for-the-badge" alt="kotlin" /></a>
</div>
<div align="center">
	<a href="https://www.android.com"><img src="https://img.shields.io/badge/package-AndroidX-brightgreen.svg?style=flat-square" alt="Platform" /></a>
	<a href="https://android-arsenal.com/api?level=6"><img src="https://img.shields.io/badge/API-6%2B-blueviolet.svg?style=flat-square" alt="API" /></a>
	<a href="http://www.apache.org/licenses/LICENSE-2.0"><img src="https://img.shields.io/badge/License-Apache%202.0-blue.svg?style=flat-square" alt="License" /></a>
</div>

# UPD: Из за закрытия JCenter вскоре эта библиотке будет перенесена в MavenCentral и скорее всего на GitHub.

## Список библиотек
- [base-error](./base-error), модуль содержащий базовые классы описания ошибок.
- [ext-bundle](./ext-bundle), модуль содержащий функции расширения для `Bundle`.
- [ext-dimensions](./ext-dimensions), модуль содержащий функции расширения для работы с `Android Dimens`.
- [ext-string](./ext-string), модуль содержащий функции расширения для `String`.
- [ext-view](./ext-view), модуль содержащий функции расширения для `View` и его наследников.

