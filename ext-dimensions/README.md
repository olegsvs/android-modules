# Dimensions Extensions

[ ![Download](https://img.shields.io/badge/latest-v1.0.8-brightgreen) ](https://bintray.com/hikkidev/android/ext-dimensions/_latestVersion)


* Number.dp2px
* Number.px2dp
* Number.px2sp
* Number.sp2px
* and transformations

Detailed: [DimentionExt.kt](https://gitlab.com/hikkidev/android-modules/blob/master/ext-dimensions/src/main/java/com/hikkidev/ext/dimensions/DimentionExt.kt)

## Setup

The module are published to bintray [repository](https://bintray.com/hikkidev/android/ext-dimensions), linked to [JCenter](https://bintray.com/bintray/jcenter?filterByPkgName=ext-dimensions).


### Gradle

Add dependencies (you can also add other modules that you need):

```gradle
dependencies {
    implementation 'com.hikkideveloper:ext-dimensions:1.0.8'
}
```
or
```kts
dependencies {
    implementation("com.hikkideveloper:ext-dimensions:1.0.8")
}
```

Make sure that you have either `jcenter()` in the list of repositories:

```gradle
repository {
    jcenter()
}
```