# Bundle Extensions

[ ![Download](https://img.shields.io/badge/latest-v1.0.8-brightgreen) ](https://bintray.com/hikkidev/android/ext-bundle/_latestVersion)

Detailed: [BundleExt.kt](https://gitlab.com/hikkidev/android-modules/blob/master/ext-bundle/src/main/java/com/hikkidev/ext/bundle/BundleExt.kt)

## Setup

The module are published to bintray [repository](https://bintray.com/hikkidev/android/ext-bundle), linked to [JCenter](https://bintray.com/bintray/jcenter?filterByPkgName=ext-bundle).


### Gradle

Add dependencies (you can also add other modules that you need):

```gradle
dependencies {
    implementation 'com.hikkideveloper:ext-bundle:1.0.8'
}
```
or
```kts
dependencies {
    implementation("com.hikkideveloper:ext-bundle:1.0.8")
}
```

Make sure that you have either `jcenter()` in the list of repositories:

```gradle
repository {
    jcenter()
}
```