# View Extensions

[ ![Download](https://img.shields.io/badge/latest-v1.1.0-brightgreen) ](https://bintray.com/hikkidev/android/ext-view/_latestVersion)


* Actions with visibility
* All padding `setPaddingLeft, setPaddingRight ...`
* Show\Hide keyboard
* TextView.underLine, TextView.deleteLine, TextView.bold
* EditText.value, EditText.passwordToggledVisible
* ViewGroup.inflate, ViewGroup.isEmpty(), ViewGroup.forEach

Detailed: [ViewExt.kt](https://gitlab.com/hikkidev/android-modules/blob/master/ext-view/src/main/java/com/hikkidev/ext/view/ViewExt.kt)

## Setup

The module are published to bintray [repository](https://bintray.com/hikkidev/android/ext-view), linked to [JCenter](https://bintray.com/bintray/jcenter?filterByPkgName=ext-view).


### Gradle

Add dependencies (you can also add other modules that you need):

```gradle
dependencies {
    implementation 'com.hikkideveloper:ext-view:1.1.0'
}
```
or
```kts
dependencies {
    implementation("com.hikkideveloper:ext-view:1.1.0")
}
```

Make sure that you have either `jcenter()` in the list of repositories:

```gradle
repository {
    jcenter()
}
```