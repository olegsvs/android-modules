# String Extensions

[ ![Download](https://img.shields.io/badge/latest-v1.0.8-brightgreen) ](https://bintray.com/hikkidev/android/ext-string/_latestVersion)


* CharSequence.isPhone()
* CharSequence.isEmail()
* CharSequence.isNumeric()
* CharSequence.md5()
* CharSequence.sha1()
* CharSequence.sha256()
* CharSequence.hexDigest(algorithm)

Detailed: [StringExt.kt](https://gitlab.com/hikkidev/android-modules/blob/master/ext-string/src/main/java/com/hikkidev/ext/string/StringExt.kt)

## Setup

The module are published to bintray [repository](https://bintray.com/hikkidev/android/ext-string), linked to [JCenter](https://bintray.com/bintray/jcenter?filterByPkgName=ext-string).


### Gradle

Add dependencies (you can also add other modules that you need):

```gradle
dependencies {
    implementation 'com.hikkideveloper:ext-string:1.0.8'
}
```
or
```kts
dependencies {
    implementation("com.hikkideveloper:ext-string:1.0.8")
}
```

Make sure that you have either `jcenter()` in the list of repositories:

```gradle
repository {
    jcenter()
}
```